import { controls } from '../../constants/controls';
import { formControlArray, checkForHitCombo } from '../helpers/controlsHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const allControls = formControlArray();
    const controlsPressed = new Set();

    const firstHealthBar = document.getElementById('left-fighter-indicator');
    const secondHealthBar = document.getElementById('right-fighter-indicator');
    firstFighter.currentHealth = firstFighter.health;
    firstFighter.canHitCombo = true;
    secondFighter.currentHealth = secondFighter.health;
    secondFighter.canHitCombo = true;

    document.addEventListener('keydown', function (event) {
      if (event.repeat) return;
      if (allControls.includes(event.code)) controlsPressed.add(event.code);

      //playerOne attack
      if (
        controlsPressed.has(controls.PlayerOneAttack) &&
        !controlsPressed.has(controls.PlayerOneBlock) &&
        !controlsPressed.has(controls.PlayerTwoBlock)
      ) {
        secondFighter.currentHealth -= getDamage(firstFighter, secondFighter);
        secondHealthBar.style.width = `${(secondFighter.currentHealth / secondFighter.health) * 100}%`;
      }

      //playerTwo attack
      if (
        controlsPressed.has(controls.PlayerTwoAttack) &&
        !controlsPressed.has(controls.PlayerTwoBlock) &&
        !controlsPressed.has(controls.PlayerOneBlock)
      ) {
        firstFighter.currentHealth -= getDamage(secondFighter, firstFighter);
        firstHealthBar.style.width = `${(firstFighter.currentHealth / firstFighter.health) * 100}%`;
      }

      //playerOne criticalHitCombination
      if (checkForHitCombo(controlsPressed, controls.PlayerOneCriticalHitCombination) && firstFighter.canHitCombo) {
        firstFighter.canHitCombo = false;
        secondFighter.currentHealth -= 2 * firstFighter.attack;
        secondHealthBar.style.width = `${(secondFighter.currentHealth / secondFighter.health) * 100}%`;
        setTimeout(() => {
          firstFighter.canHitCombo = true;
        }, 10000);
      }

      //playerTwo criticalHitCombination
      if (checkForHitCombo(controlsPressed, controls.PlayerTwoCriticalHitCombination) && secondFighter.canHitCombo) {
        secondFighter.canHitCombo = false;
        firstFighter.currentHealth -= 2 * secondFighter.attack;
        firstHealthBar.style.width = `${(firstFighter.currentHealth / firstFighter.health) * 100}%`;
        setTimeout(() => {
          secondFighter.canHitCombo = true;
        }, 10000);
      }

      if (firstFighter.currentHealth <= 0) resolve(secondFighter);
      else if (secondFighter.currentHealth <= 0) resolve(firstFighter);
    });

    document.addEventListener('keyup', function (event) {
      if (allControls.includes(event.code)) controlsPressed.delete(event.code);
    });
  });
}

export function getDamage(attacker, defender) {
  let damage;
  const attack = getHitPower(attacker);
  const defend = getBlockPower(defender);

  if (defend >= attack) damage = 0;
  else damage = attack - defend;

  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
