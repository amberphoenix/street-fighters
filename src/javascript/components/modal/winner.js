import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: fighter.name,
    bodyElement: 'Goood job!',
    onClose: () => {
      document.location.reload();
    },
  });
}
