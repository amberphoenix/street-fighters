import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    fighterElement.appendChild(fighterImage);

    const fighterInfo = createElement({
      tagName: 'div',
      className: 'fighter-preview__info',
    });

    const fighterList = createElement({
      tagName: 'ul',
      className: 'fighter-preview__list',
    });

    const fighterChars = ['name', 'health', 'attack', 'defense'].map((key) => {
      const el = createElement({ tagName: 'li' });
      el.innerText = `${key}: ${fighter[key]}`;
      return el;
    });

    fighterChars.map((fighter) => fighterList.appendChild(fighter));
    fighterInfo.appendChild(fighterList);
    fighterElement.appendChild(fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
