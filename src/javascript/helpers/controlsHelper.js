import { controls } from '../../constants/controls';

export const formControlArray = () => {
  const allControls = [];
  Object.keys(controls).forEach((key) => {
    Array.isArray(controls[key]) ? allControls.push(...controls[key]) : allControls.push(controls[key]);
  });
  return allControls;
};

export const checkForHitCombo = (activeControls, combination) => {
  return combination.reduce((canHit, c) => {
    return canHit && activeControls.has(c);
  }, true);
};
